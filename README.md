# Contacts List SPA

### Installation

To run this project clone this repo, in terminal navigate to the project directory and run:
` npm install `

After all the installation run:
` npm run dev `
and open http://localhost/1234 in your browser.

### About the app

Contacts will be shown in alphabetical order - 20 records per page. You can filter them by first/last name by entering the search term in the input above the list.

### Stack

ReactJS
Axios
Styled Components
Parcel Bundler