export const apiUrl =
  'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json';

export const imageUrl =
  'https://upload.wikimedia.org/wikipedia/commons/8/89/Portrait_Placeholder.png';

export const icon =
  'https://lh3.googleusercontent.com/proxy/EK94V6kKcQmW2WeMjS63w1iBZq_BbWJcMzOo7D-NyO6oG-KVFn4k7m3pSRTMZZBgYtCEwTH-FxAEqwHb9krr_ryzwA05e-wiLg-jfD8XuCZoORfnw3PBx8kroWcuP3SjeesWjpvNSVJxIIL1TF51BdMPk_qKYrFworKzfkKW0LPw0w';
