import React from 'react';
import { Navbar } from './components/Navbar';
import { ThemeProvider } from 'styled-components';
import { theme } from './styles/theme';
import { GlobalStyle } from './styles/GlobalStyle';
import { ContactsList } from './components/ContactsList';


export const App = () => {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <Navbar />
      <ContactsList />
    </ThemeProvider>
  );
};
