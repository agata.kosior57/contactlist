export const sortByLastName = (users) => {
  return users.sort((a, b) => {
    if (a.last_name < b.last_name) return -1;
    if (a.last_name > b.last_name) return 1;
    return 0;
  });
};

export const filretBySearchTerm = (contacts, searchTerm) => {
  return contacts.filter(
    (contact) =>
      contact.first_name.toLowerCase().includes(searchTerm) ||
      contact.last_name.toLowerCase().includes(searchTerm)
  );
};
