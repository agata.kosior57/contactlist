export const theme = {
  colors: {
    white: '#ffffff',
    black: '#000000',
    grey: '#dadada',
    seablue: '#5eb0be',
  },
  linearGradient: 'linear-gradient(to right, #5eb0be, #4ead90)',
};
