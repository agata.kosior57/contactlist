import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const SPagination = styled.div`
  margin: 2rem auto;
`;

const SPageNumber = styled.button`
  padding: 0.25rem 0.7rem;
  height: 2.5rem;
  margin: 0.2rem;
  font-size: 1rem;
  border: 1px solid
    ${({ theme, current }) =>
      current ? theme.colors.seablue : theme.colors.grey};
  color: ${({ theme, current }) =>
    current ? theme.colors.seablue : theme.colors.black};
  cursor: pointer;
`;

export const Pagination = ({ numberOfPages, currentPage, setPage }) => {
  const renderPageNumbers = () => {
    let pages;
    const lastPage = numberOfPages;
    if (numberOfPages < 6) {
      pages = Array(numberOfPages)
        .fill(0)
        .map((a, i) => i + 1);
    } else if (currentPage < 3) {
      pages = [1, 2, 3, '...', lastPage];
    } else if (
      (numberOfPages > 3 && currentPage <= lastPage - 3) ||
      currentPage <= lastPage - 3
    ) {
      pages = [1, '...', currentPage, '...', lastPage];
    } else {
      pages = [1, '...', lastPage - 2, lastPage - 1, lastPage];
    }

    const handleClick = (e) => {
      setPage(e.target.name);
    };

    return pages.map((page, i) => (
      <SPageNumber
        current={page === currentPage}
        key={page + i}
        name={page}
        onClick={handleClick}
        disabled={page === '...'}
      >
        {page}
      </SPageNumber>
    ));
  };
  return (
    <SPagination>
      <SPageNumber
        onClick={() => setPage(currentPage - 1)}
        disabled={currentPage === 1}
      >
        Prev Page
      </SPageNumber>
      {renderPageNumbers()}
      <SPageNumber
        onClick={() => setPage(currentPage + 1)}
        disabled={currentPage === numberOfPages}
      >
        Next Page
      </SPageNumber>
    </SPagination>
  );
};

Pagination.propTypes = {
  numberOfPages: PropTypes.number.isRequired,
  currentPage: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
};
