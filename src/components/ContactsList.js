import React, { useState, useEffect } from 'react';
import axios from 'axios';
import styled from 'styled-components';
import { Contact } from './Contact';
import { Pagination } from './Pagination';
import { apiUrl } from '../constants';
import { SearchBar } from './SearchBar';
import { sortByLastName } from '../utils';

const SContactsList = styled.div`
  display: flex;
  flex-direction: column;
  max-width: 450px;
  width: 90%;
  margin: 2rem auto;
`;

export const ContactsList = () => {
  const [contacts, setContacts] = useState([]);
  const [contactsToRender, setContactsToRender] = useState([]);
  const [page, setPage] = useState(1);
  const [numberOfPages, setNumberOfPages] = useState(0);
  const [searchTerm, setSearchTerm] = useState('');
  const [chosenContacts, setChosenContacts] = useState([]);

  const LIMIT = 20;

  // if the backend provided pagination I would send a request for a chosen page instead, every time the page would change

  useEffect(() => {
    axios
      .get(apiUrl)
      .then((res) => {
        setNumberOfPages(Math.ceil(res.data.length / LIMIT));
        const data = sortByLastName(res.data);
        setContacts(data);
      })
      .catch((error) => console.log(error));
  }, []);

  useEffect(() => {
    console.log(chosenContacts);
  }, [chosenContacts]);

  useEffect(() => {
    let contactsToSet = contacts;
    if (searchTerm) {
      contactsToSet = filretBySearchTerm(contacts, searchTerm);
      setNumberOfPages(Math.ceil(contactsToSet.length / LIMIT));
    } else {
      setNumberOfPages(Math.ceil(contacts.length / LIMIT));
    }
    setContactsToRender(contactsToSet.slice((page - 1) * LIMIT, page * LIMIT));
  }, [page, searchTerm, contacts]);

  const chooseContacts = (contact) => {
    if (chosenContacts.includes(contact)) {
      setChosenContacts([
        ...chosenContacts.filter((chosen) => chosen != contact),
      ]);
    } else {
      setChosenContacts([...chosenContacts, contact]);
    }
  };

  const renderContacts = () => {
    return contactsToRender.map((contact) => (
      <Contact
        firstName={contact.first_name}
        lastName={contact.last_name}
        email={contact.email}
        avatar={contact.avatar || undefined}
        id={contact.id}
        chooseContacts={chooseContacts}
        key={contact.id}
      />
    ));
  };

  return (
    <SContactsList>
      <SearchBar setSearchTerm={setSearchTerm} />
      {renderContacts()}
      <Pagination
        numberOfPages={numberOfPages}
        currentPage={page}
        setPage={setPage}
      />
    </SContactsList>
  );
};
