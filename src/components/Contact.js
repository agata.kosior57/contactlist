import React from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { imageUrl } from '../constants';

const SContact = styled.div`
  display: flex;
  border-bottom: 1px solid ${({ theme }) => theme.colors.grey};
  align-items: center;
  justify-content: space-between;
  padding: 0.5rem;
  cursor: default;
`;

const SAvatar = styled.img`
  height: 48px;
  width: 48px;
  border-radius: 50%;
  object-fit: cover;
  margin-right: 2rem;
`;

const SUserInfo = styled.div`
  flex: 1;
`;

export const Contact = ({
  avatar,
  firstName,
  lastName,
  email,
  id,
  chooseContacts,
}) => {
  return (
    <SContact>
      <SAvatar src={avatar} alt='Contact avatar' />
      <SUserInfo>
        <h3>{`${firstName} ${lastName}`}</h3>
        <span>{email}</span>
      </SUserInfo>
      <input
        type='checkbox'
        name='check'
        id={id}
        onChange={(e) => chooseContacts(e.target.id)}
      />
    </SContact>
  );
};

Contact.propTypes = {
  avatar: PropTypes.string,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  chooseContacts: PropTypes.func.isRequired,
};

Contact.defaultProps = {
  avatar: imageUrl,
};
