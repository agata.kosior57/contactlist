import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { icon } from '../constants';

const SInput = styled.input`
  width: 100%;
  height: 2rem;
  appearance: none;
  border-radius: 0.25rem;
  padding: 0.25rem 0.5rem;
  border: 1px solid ${({ theme }) => theme.colors.grey};
  background: url(${icon}) no-repeat center right 20px/20px;
`;

export const SearchBar = ({ setSearchTerm }) => {
  return (
    <SInput
      type='text'
      name='searchbar'
      id='searchbar'
      onChange={(e) => setSearchTerm(e.target.value.toLowerCase())}
    />
  );
};

SearchBar.propTypes = {
  setSearchTerm: PropTypes.func.isRequired,
};
