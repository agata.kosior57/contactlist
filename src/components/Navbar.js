import React from 'react';
import styled from 'styled-components';

const SNavbar = styled.div`
  width: 100%;
  background: ${({ theme }) => theme.linearGradient};
  height: 4rem;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1rem;
  color: ${({ theme }) => theme.colors.white};
`;

export const Navbar = () => {
  return (
    <SNavbar>
      <h1>Contacts</h1>
    </SNavbar>
  );
};
